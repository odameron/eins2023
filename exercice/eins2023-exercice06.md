---
title: "Exercice 6: take ontologies into account for writing semantically-rich SPARQL queries"
tags: eins2023, teaching, RDF, ontologies, RDFS, OWL, SPARQL
version: 1.0
date: 2023-07-05
---


# 6.0 Load the data and knowledge

We will use:
- [`exercice06.ttl`](exercice06.ttl) a slightly modified version of the data from [Exercice04](eins2023-exercice04.md) with all the proteins annotated by different classes from the GeneOntology
- [`goSimple.ttl`](goSimple.ttl) a simplified version of the GeneOntology for representing the hierarchy underlying the classes from the previous step

Clear the content of your GraphDB repository, and load these two files.

![Procedure for clearing the content of a GraphDB repository](../figures/graphdb-clearRepository-annotated.png)


![Union of a data graph representing the annotations of five proteins with a knowledge graph representing the knowledge underlying these annotations](../figures/goAnnotations-hierarchy.png)


# 6.1 Write the SPARQL query that retrieves the direct annotation(s) and their labels of protein p4

Expected result:

| ?go | ?goLabel |
| --- | --- |
|[go:0035172](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0035172 "http://purl.obolibrary.org/obo/GO_0035172")|"hematocyte prolif"|
|[go:0006006](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0006006 "http://purl.obolibrary.org/obo/GO_0006006")|"glucose metab proc"|

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/Z28gP2dvTGFiZWwKV0hFUkUgewogIGV4OnA0IGdvYXZvYzpwcm9jZXNzID9nbyAuCiAgP2dvIHJkZnM6bGFiZWwgP2dvTGFiZWwgLgp9Cg==" | base64 --decode
```


# 6.2 Write the SPARQL query that retrieves the direct annotation(s) of protein p1, and all their superclasses and their labels

Expected result:

| ?go | ?goLabel |
|---|---|
|[go:0005997](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0005997 "http://purl.obolibrary.org/obo/GO_0005997")|"xylulose metab proc"|
|[go:0005996](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0005996 "http://purl.obolibrary.org/obo/GO_0005996")|"monosaccharide metab proc"|
|[go:0008150](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0008150 "http://purl.obolibrary.org/obo/GO_0008150")|"BP"|
|[go:0008152](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0008152 "http://purl.obolibrary.org/obo/GO_0008152")|"metab proc"|
|[go:0019321](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0019321 "http://purl.obolibrary.org/obo/GO_0019321")|"pentose metab proc"|

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/Z28gP2dvTGFiZWwKV0hFUkUgewogIGV4OnAxIGdvYXZvYzpwcm9jZXNzID9nb0RpcmVjdCAuCiAgP2dvRGlyZWN0IHJkZnM6c3ViQ2xhc3NPZiogP2dvIC4KICA/Z28gcmRmczpsYWJlbCA/Z29MYWJlbCAuCn0K" | base64 --decode
```


# 6.3 Write the SPARQL query that retrieves the proteins annotated by "Monosaccharide metabolic process" , and their names

Expected result:

| ?protein | ?proteinLabel |
|---|---|
|[ex:p3](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp3 "http://example.org/tutorial/p3")|"Protein P3"|
|[ex:p1](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp1 "http://example.org/tutorial/p1")|"Protein P1"|
|[ex:p2](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp2 "http://example.org/tutorial/p2")|"Protein P2"|
|[ex:p4](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp4 "http://example.org/tutorial/p4")|"Protein P4"|

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/cHJvdGVpbiA/cHJvdGVpbkxhYmVsCldIRVJFIHsKICA/cHJvdGVpbiByZGY6dHlwZSBleDpQcm90ZWluIC4KICA/cHJvdGVpbiByZGZzOmxhYmVsID9wcm90ZWluTGFiZWwgLgogID9wcm90ZWluIGdvYXZvYzpwcm9jZXNzID9nb0RpcmVjdCAuCiAgP2dvRGlyZWN0IHJkZnM6c3ViQ2xhc3NPZiogP2dvIC4KICA/Z28gcmRmczpsYWJlbCAibW9ub3NhY2NoYXJpZGUgbWV0YWIgcHJvYyIgLgp9Cg==" | base64 --decode
```
