---
title: "Exercice 7: real-world application: use DisGeNET to identify the genes associated to a (class of) disease(s)"
version: 1.0
date: 2023-08-28
tags: [eins2023, teaching, RDF, ontologies, RDFS, OWL, SPARQL, disease, DisGeNET]
---


# 7.0 DisGeNET

DisGeNET (https://www.disgenet.org/home/) is a public collection of genes and variants associated to human diseases. DisGeNET (v7.0) contains 1,134,942 gene-disease associations (GDAs), between 21,671 genes and 30,170 diseases.

It is available in RDF, and has mappings to biomedical ontologies sur as the Human Disease Ontology. It also has a public SPARQL endpoint (http://rdf.disgenet.org/sparql/).

![DisGeNET RDF schema](../figures/disgenet_rdf_schema_v7.png)


# 7.1 Find the genes directly associated to familial hyperlipidemia (`doid:1168`)

```sparql
# 03 diseases that are exact matches of doid:1168
SELECT DISTINCT ?disease ?diseaseLabel
FROM <http://rdf.disgenet.org> 
WHERE {
  ?disease rdf:type ncit:C7057 .
  OPTIONAL { ?disease rdfs:label ?diseaseLabel }
  ?disease skos:exactMatch <http://purl.obolibrary.org/obo/DOID_1168> .
}
```


# 7.2 Explore the subclasses of familial hyperlipidemia (`doid:1168`)

## Descendants of `doid:1168`

```sparql
SELECT DISTINCT ?disease ?diseaseLabel 
FROM <http://rdf.disgenet.org> 
WHERE {
  ?disease rdfs:subClassOf* <http://purl.obolibrary.org/obo/DOID_1168> .
  OPTIONAL { ?disease rdfs:label ?diseaseLabel }
}
ORDER BY ?disease
```

## Hierarchy of the descendants of familial hyperlipidemia (`doid:1168`)

```sparql
SELECT DISTINCT ?diseaseParent ?diseaseParentLabel ?disease ?diseaseLabel
FROM <http://rdf.disgenet.org> 
WHERE {
  ?diseaseParent rdfs:subClassOf* <http://purl.obolibrary.org/obo/DOID_1168> .
  OPTIONAL { ?diseaseParent rdfs:label ?diseaseParentLabel }
  ?disease rdfs:subClassOf ?diseaseParent .
  OPTIONAL { ?disease rdfs:label ?diseaseLabel }
}
ORDER BY ?diseaseParent ?disease
```

NB: using the convenient rdfCartographer (https://gitlab.com/odameron/rdfCartographer) the following script generates the diagram

```python
#! /usr/bin/env python3

import RdfCartographer

cartographer = RdfCartographer.RdfCartographer('http://rdf.disgenet.org/sparql/')
cartographer.add_prefix("doid", "http://purl.obolibrary.org/obo/DOID_")

cartographer.add_relation_transitive_links("doid:1168", "rdfs:subClassOf", relationDirection = "Inbound", includeLabel = True, includeType = False, filterType="owl:Class")
cartographer.save_RDF_graph("disgenet-familialHyperlipidemia-hierarchy.ttl", displayImageGenerationInstruction=True)

# rapper --input turtle disgenet-familialHyperlipidemia-hierarchy.ttl --output dot | dot -Tpng -o disgenet-familialHyperlipidemia-hierarchy.png
```

![Hierarchy of the subclasses of Familial hyperlipidemia (doid:1168)](figures/disgenet-familialHyperlipidemia-hierarchy.png)


# 7.3 Find the genes (in-)directly associated to familial hyperlipidemia (`doid:1168`)

```sparql
SELECT DISTINCT ?gene ?geneLabel
#SELECT (COUNT(DISTINCT ?gene) AS ?nbGenes)
#SELECT DISTINCT ?gda ?disease ?diseaseLabel ?gene 
FROM <http://rdf.disgenet.org> 
WHERE {
  ?gda sio:SIO_000628 ?disease,?gene .
  ?disease rdf:type ncit:C7057 .
  OPTIONAL { ?disease rdfs:label ?diseaseLabel }
  ?disease skos:exactMatch/(rdfs:subClassOf*) <http://purl.obolibrary.org/obo/DOID_1168> .
  #filter regex(?disease, "umls/id") .
  ?gene rdf:type ncit:C16612 .
  #filter regex(?gene, "ncbigene") . 
  OPTIONAL { ?gene rdfs:label ?geneLabel }
} 
```


# 7.4 Map the genes to the hierarchy of diseases and analyze the overlap

```sparql
SELECT DISTINCT ?disease ?diseaseLabel (COUNT(DISTINCT ?gene) AS ?nbGenes)
FROM <http://rdf.disgenet.org> 
WHERE {
  ?disease rdfs:subClassOf* <http://purl.obolibrary.org/obo/DOID_1168> .
  OPTIONAL { ?disease rdfs:label ?diseaseLabel }
  
  ?diseaseDirect skos:exactMatch/(rdfs:subClassOf*) ?disease .
  ?gda sio:SIO_000628 ?diseaseDirect .
  ?diseaseDirect rdf:type ncit:C7057 .
  ?gda sio:SIO_000628 ?gene .
}
ORDER BY DESC(?nbGenes) ?disease
```

|disease|diseaseLabel|nbGenes|
|---|---|---|
|[http://purl.obolibrary.org/obo/DOID_1168](http://purl.obolibrary.org/obo/DOID_1168)|"familial hyperlipidemia"^^<http://www.w3.org/2001/XMLSchema#string>|2449|
|[http://purl.obolibrary.org/obo/DOID_13810](http://purl.obolibrary.org/obo/DOID_13810)|"familial hypercholesterolemia"^^<http://www.w3.org/2001/XMLSchema#string>|1808|
|[http://purl.obolibrary.org/obo/DOID_0090105](http://purl.obolibrary.org/obo/DOID_0090105)|"autosomal recessive hypercholesterolemia"^^<http://www.w3.org/2001/XMLSchema#string>|120|
|[http://purl.obolibrary.org/obo/DOID_13809](http://purl.obolibrary.org/obo/DOID_13809)|"familial combined hyperlipidemia"^^<http://www.w3.org/2001/XMLSchema#string>|120|
|[http://purl.obolibrary.org/obo/DOID_14118](http://purl.obolibrary.org/obo/DOID_14118)|"familial lipoprotein lipase deficiency"^^<http://www.w3.org/2001/XMLSchema#string>|116|
|[http://purl.obolibrary.org/obo/DOID_3145](http://purl.obolibrary.org/obo/DOID_3145)|"hyperlipoproteinemia type III"^^<http://www.w3.org/2001/XMLSchema#string>|59|
|[http://purl.obolibrary.org/obo/DOID_1172](http://purl.obolibrary.org/obo/DOID_1172)|"hyperlipoproteinemia type IV"^^<http://www.w3.org/2001/XMLSchema#string>|15|
|[http://purl.obolibrary.org/obo/DOID_1171](http://purl.obolibrary.org/obo/DOID_1171)|"hyperlipoproteinemia type V"^^<http://www.w3.org/2001/XMLSchema#string>|8|

